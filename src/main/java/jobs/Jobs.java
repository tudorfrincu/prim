package jobs;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Jobs {

    public static void main(String[] args) {

        List<Task> tasks = Arrays.asList(new Task(1, 9, 15), new Task(2, 2, 2), new Task(3, 5, 18), new Task(4, 7, 1),
                new Task(5, 4, 25), new Task(6, 2, 20), new Task(7, 5, 8), new Task(8, 7, 10), new Task(9, 4, 12),
                new Task(10, 3, 5));

        Task[] takenTasks = new Task[tasks.size()];

        Task emptySlot = new Task(-1, -1, -1);

        Arrays.fill(takenTasks, emptySlot);

        List<Task> sortedArray = sortArray(tasks);

        for (Task task : sortedArray) {

            int index = task.getDeadline() - 1;

            if (takenTasks[index] == emptySlot) {
                takenTasks[index] = task;
            } else {
                while (index > 0) {
                    index--;
                    if (takenTasks[index] == emptySlot) {
                        takenTasks[index] = task;
                        break;
                    }
                }
            }
        }

        int suma = 0;

        for (int i = 0; i < takenTasks.length; i++)

            if (takenTasks[i] == emptySlot) {
                System.out.println("Empty");
            } else {
                System.out.println(takenTasks[i].getTaskId() + "----------" + takenTasks[i]);
                suma += takenTasks[i].getProfit();
            }

        System.out.println("Suma: " + suma);

    }

    public static List<Task> sortArray(List<Task> arrayToSort) {
        return arrayToSort.stream().sorted(Comparator.comparing(Task::getProfit).reversed())
                .collect(Collectors.toList());

    }

}
