package prim;

class Prim
{

    private static final int VARFURI = 9;

    int minKey(int key[], Boolean mstSet[])
    {

        int min = Integer.MAX_VALUE, min_index = -1;

        for (int v = 0; v < VARFURI; v++)
            if (mstSet[v] == false && key[v] < min)
            {
                min = key[v];
                min_index = v;
            }

        return min_index;
    }

    void afisare(int parinte[], int n, int graph[][])
    {
        System.out.println("Muchii \tCost");
        for (int i = 1; i < VARFURI; i++)
            System.out.println(parinte[i] + " - " + i + "\t" +
                graph[i][parinte[i]]);
    }

    void prim(int graph[][])
    {
        int parinte[] = new int[VARFURI];

        int key[] = new int[VARFURI];

        Boolean mstSet[] = new Boolean[VARFURI];

        for (int i = 0; i < VARFURI; i++)
        {
            key[i] = Integer.MAX_VALUE;
            mstSet[i] = false;
        }

        key[0] = 0;

        parinte[0] = -1;

        for (int count = 0; count < VARFURI - 1; count++)
        {

            int u = minKey(key, mstSet);

            mstSet[u] = true;

            for (int v = 0; v < VARFURI; v++)

                if (graph[u][v] != 0 && mstSet[v] == false &&
                    graph[u][v] < key[v])
                {
                    parinte[v] = u;
                    key[v] = graph[u][v];
                }
        }

        afisare(parinte, VARFURI, graph);
    }

    public static void main(String[] args)
    {
   
        Prim t = new Prim();
        
        int graph[][] = new int[][]{{0, 4, 0, 0, 0, 0, 0, 8, 0},
            {4, 0, 8, 0, 0, 0, 0, 11, 0},
            {0, 8, 0, 7, 0, 4, 0, 0, 2},
            {0, 0, 7, 0, 9, 14, 0, 0, 0},
            {0, 0, 0, 9, 0, 10, 0, 0, 0},
            {0, 0, 4, 14, 10, 0, 2, 0, 0},
            {0, 0, 0, 0, 0, 2, 0, 1, 6},
            {8, 11, 0, 0, 0, 0, 1, 0, 7},
            {0, 0, 2, 0, 0, 0, 6, 7, 0}};

        t.prim(graph);
    }
}